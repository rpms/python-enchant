import enchant
from enchant.checker import SpellChecker
from enchant.tokenize import (get_tokenizer, HTMLChunker, EmailFilter)
import unittest
import sys

class TestPyenchant(unittest.TestCase):
    
    def test_check_dict(self):
        d = enchant.Dict("en_US")
        self.assertEqual(d.tag, 'en_US')
        # invalid test string
        result = d.check("こんにちは")
        self.assertFalse(result)
        # valid test sting
        result = d.check("Hello")
        self.assertTrue(result)    

    def test_dict_exist(self):
        # invalid locale
        result = enchant.dict_exists("de_kk")
        self.assertFalse(result)
        # valid locale
        result = enchant.dict_exists("en_US")
        self.assertTrue(result)


    def test_suggestion(self):
        d = enchant.Dict("en_US")
        # valid string sugesstion
        result = d.suggest("Hello")
        self.assertGreater(len(result),0)
        # invalid string sugesstion 
        result = d.suggest("こんにちは")
        self.assertEqual(len(result), 0)


    def test_personal_word_list(self):
        d = enchant.DictWithPWL("en_US", "mywords.txt")
        
        result = d.check("estivation")
        self.assertTrue(result)

        suggestion_list = d.suggest('estivateion')
        self.assertIn("estivation", suggestion_list)
    

    def test_check_entire_block(self):
        chkr = SpellChecker("en_US")
        chkr.set_text("This is erors text.")
        errorlist = list()
        for err in chkr:
            errorlist.append(err.word)
        self.assertIn("erors", errorlist)
    

    def test_tokenization(self):
        tknzr = get_tokenizer("en_US")
        tknzr_list = [word for word in tknzr("Hello world")]
        self.assertEqual(tknzr_list, [('Hello', 0), ('world', 6)])
    

    def test_chunkers(self):
        tknzr = get_tokenizer("en_US",chunkers=(HTMLChunker,))
        tknzr_list = [w for w in tknzr("Hello <span class='important'>World</span>")]
        self.assertEqual(tknzr_list ,[('Hello', 0), ('World', 30)])
    

# logger function
def main(out = sys.stderr, verbosity = 2):
    loader = unittest.TestLoader()
    suite = loader.loadTestsFromModule(sys.modules[__name__]) 
    unittest.TextTestRunner(out, verbosity = verbosity).run(suite)

if __name__ == "__main__":
    test_log_path = sys.argv[1] if len(sys.argv)>1 else "test.log"
    with open(test_log_path, 'w') as f: 
        main(f)
    
    # unittest.main()